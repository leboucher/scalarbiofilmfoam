# Process for the biofilm model

## Add a consumed, growing scalar field

The growing scalar field is called X and is expressed as the following
$$
\frac{\partial X}{\partial t}\, + \nabla \cdot \left(XU_\alpha\right) - \nabla^2 \left(\mathcal{D}_\alpha X\right) = \, \alpha_2 \left[ k_1 X \frac{S}{K + S} - k_2X \right ]
$$


The consumption term for the S field (soluble) is the following
$$
\frac{\partial S}{\partial t} + \nabla \cdot \left( SU_\alpha \right) - \nabla^2 \left( \mathcal{D}_\alpha S \right) = \, \alpha_2 \left[ k_2X - k_1X\frac{S}{K + S} \right]
$$


Although these equations aren't realistic, we can see how they evolve in time, and we have an easy way to segregate the growth of $X$ to the phase field $\alpha_2$.  This is interesting as we need to do this for the full photoanaerobic biofilm model.

## Resolving the interface to avoid spurious diffusion

As the biofilm needs to be well defined at the interface, we introduced a term based on the algorithm provided by Haroun, 2010. This algorithm adds an extra divergence term for flow across the boundary, and is parametrised by a dimensionless coefficient, which we will call $C_{id}​$ (interfacial dampener coefficient).

## Couple the growth of X to the volume fraction fields

This process will extend that as described by Gim 2016 where the growth of biological species is coupled to the biofilm phase fraction. As the biofilm species grows, it pushes the alpha field. We can only achieve this by defining the surface (or volume in the context of cells) available for this to occur.

An OpenFOAM implementation of a similar phenomena was described by Kreken Almeland, 2018 where the turbulence energy was given as a source term to the volume fraction field.

We will therefore draw upon both of these methods in order to develop our biofilm model. We will keep the $\alpha$ field as liquid, and the secondary field as biofilm. This means that positive growth will take away from the $\alpha$ field. 
$$

$$
